//
//  JsonHelper.swift
//  SANDBX_Test
//
//  Created by Alex on 09.04.18.
//

import Foundation

class JsonHelper {
    
    private let decoder = JSONDecoder()
    private let kFileName = "info"
    private let kFileType = "json"
    
    private func dataFromFile(_ filename: String) -> Data? {
        let bundle = Bundle.main
        let path = bundle.path(forResource: filename, ofType: kFileType)
        return path == nil ? nil : try? Data(contentsOf: URL(fileURLWithPath: path!))
    }
    
    private func serializeToResponse(data: Data, anError: inout Error?) -> [Car]? {
        var theResponse : Cars?
        do {
            theResponse = try decoder.decode(Cars.self, from: data)
        } catch {
            print("Serialization failed. Error: \(error)")
        }
        return theResponse?.cars
    }
    
    func getModels() -> [Car]? {
        if let data = dataFromFile(kFileName) {
            var anError: Error? = nil
            return serializeToResponse(data: data, anError: &anError)
        }
        assertionFailure("Failed to retrive data ffrom file.")
        return nil
    }
}
