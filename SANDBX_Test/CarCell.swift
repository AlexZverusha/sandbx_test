//
//  CarCell.swift
//  SANDBX_Test
//
//  Created by Alex on 08.04.18.
//

import UIKit

class CarCell: UITableViewCell {
    
    @IBOutlet private weak var car_modelTextLabel: UILabel!
    @IBOutlet private weak var car_typeTextLabel: UILabel!
    @IBOutlet private weak var car_idTextLabel: UILabel!
    @IBOutlet private weak var car_colorTextLabel: UILabel!
    
    @IBOutlet private weak var owner_idTextLabel: UILabel!
    @IBOutlet private weak var owner_nameTextLabel: UILabel!
    @IBOutlet private weak var owner_phoneTextLabel: UILabel!
    @IBOutlet private weak var ownerStackView: UIStackView!
    
    var item: ListViewModelItem? {
        
        didSet {
            guard let item = item as? ListViewCommonlItem else {
                assertionFailure()
                return
            }
            if let textId = item.car_id {
                car_idTextLabel.text = String(textId)
            }
            
            car_modelTextLabel.text = item.car_model
            car_typeTextLabel.text = item.car_type
            car_colorTextLabel.text = item.car_color
            
            if let textOwnerId = item.owner_id {
                owner_idTextLabel.text = String(textOwnerId)
            }
            
            owner_nameTextLabel.text = item.owner_name
            owner_phoneTextLabel.text = item.owner_phone
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        ownerStackView.isHidden = !selected
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        ownerStackView.isHidden = true
    }
}
