//
//  Model.swift
//  SANDBX_Test
//
//  Created by Alex on 08.04.18.
//

import Foundation

protocol ListViewModelItem { }

struct ListViewCommonlItem: ListViewModelItem {
    var car_id: Int?
    var car_type: String?
    var car_model: String?
    var car_color: String?
    var owner_id: Int?
    var owner_name: String?
    var owner_phone: String?
    
    init(model: Car) {
        self.car_id = model.car_id
        self.car_type = model.car_type
        self.car_model = model.car_model
        self.car_color = model.car_color
        let owner = model.owner?.first
        self.owner_id = owner?.owner_id
        self.owner_name = owner?.owner_name
        self.owner_phone = owner?.owner_phone
    }
}

struct Car: Codable {
    var car_id: Int?
    var car_type: String?
    var car_model: String?
    var car_color: String?
    var owner: [Owner]?
    
    enum CodingKeys: String, CodingKey {
        case car_id
        case car_type
        case car_model
        case car_color
        case owner = "owners"
    }
}

struct Cars: Codable {
    var cars: [Car]?
    enum CodingKeys: String, CodingKey {
         case cars = "data"
    }
}

struct Owner: Codable {
    var owner_id: Int?
    var owner_name: String?
    var owner_phone: String?

    enum CodingKeys: String, CodingKey {
        case owner_id
        case owner_name
        case owner_phone
    }
}



