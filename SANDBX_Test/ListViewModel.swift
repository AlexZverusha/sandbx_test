//
//  ListViewModel.swift
//  SANDBX_Test
//
//  Created by Alex on 08.04.18.
//

import UIKit

class ListViewModel: NSObject {
    private var items = [ListViewModelItem]()
    private let kNumberOfSectionsInTableView = 1
    private let kCellIdentifier = "cell"
    
    override init() {
        super.init()
        if let list = JsonHelper().getModels() {
            list.forEach { items.append(ListViewCommonlItem(model: $0)) }
        }
    }
}

extension ListViewModel: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return kNumberOfSectionsInTableView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier, for: indexPath) as? CarCell {
            cell.item = items[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
}

extension ListViewModel: UITableViewDelegate {
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            tableView.beginUpdates()
            tableView.endUpdates()
    }
}
