//
//  ViewController.swift
//  SANDBX_Test
//
//  Created by Alex on 08.04.18.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView?
    private let viewModel = ListViewModel()
    private let kEstimatedRowHeight: CGFloat = 100

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView?.dataSource = viewModel
        tableView?.delegate = viewModel
        tableView?.rowHeight = UITableViewAutomaticDimension
        tableView?.estimatedRowHeight = kEstimatedRowHeight
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView?.reloadData()
    }
}


